# Game of Death that Kills

<aside>
💀 Game of Death that Kills est un jeu multijoueur de type FPS dit "jeu de tir à la première personne" réalisé par Isamettin AYDIN, Bastian FABRE et Arthur VILLARD.

</aside>

# Histoire du jeu

---

Plongé dans une simulation en réalité augmentée, vous vous entraînez pour la Troisième Guerre Mondiale qui menace le monde depuis plusieurs années. Dans une ville artificielle à l’échelle, vous affrontez vos camarades avec des armes réalistes et réalisez le plus de kills possible. La dernière technologie développée par la France en prévention de ces conflits mondiaux est le jetpack, engin permettant de voler quelque temps dans les airs, ce qui pourra certainement modifier le cours de la bataille…

# But du jeu

---

Le but du jeu est de se déplacer dans la ville, traquer vos ennemis et les abattre. Vous réapparaîtrez à chaque mort jusqu'à la fin de la partie. La partie se finit au bout de trois minutes de jeu et le joueur avec le plus de kills gagne.

# Contrôles

---

![keys_godtk.png](Game%20of%20Death%20that%20Kills/keys_godtk.png)

# 🖥️Interface et fonctionnalités

---

> Au lancement du jeu, vous devez tout d’abord choisir un pseudonyme pour votre session de jeu.
> 

![Premier écran du jeu : Choix du pseudonyme](Game%20of%20Death%20that%20Kills/pseudo_godtk.png)

    Premier écran du jeu : Choix du pseudonyme

> Vous pouvez ensuite choisir d’héberger une partie ou d’en rejoindre une en spécifiant son adresse. Il est aussi possible de changer son pseudonyme.
> 

![Deuxième écran du jeu : Démarrage de la partie](Game%20of%20Death%20that%20Kills/menu_godtk.png)

    Deuxième écran du jeu : Démarrage de la partie

> Une fois une partie lancée, vous vous retrouvez directement en jeu.
Vous avez la possibilité de consulter le tableau des scores ou d’afficher le menu pause permettant de quitter la partie.
> 

![](Game%20of%20Death%20that%20Kills/jeu_godtk.png)

    minimap (en jaune), la barre de carburant et de vie (en vert), la barre de munitions (en rouge), la barre de vie d’adversaire (en bleu), le tableau de score (en magenta), le temps (en orange) et l’arme à droite.
![Menu Pause](Game%20of%20Death%20that%20Kills/pause_godtk.png)

    Menu Pause

---

# 🔫 Arme

> Votre arme ne dispose que de 30 munitions, mais vous pouvez la recharger à tout moment. Quand l’arme ne dispose plus de munitions, elle se recharge automatiquement. Vous pouvez tout de même penser à la recharger entre deux affronts ou anticiper cette recharge.
> 

# 🚀 Jetpack

> La nouvelle invention militaire en date est disponible dans ce jeu. Vous pouvez donc voler librement dans la limite de la capacité en carburant de votre jetpack. Cette fonctionnalité dynamise les déplacements et ajoute des opportunités différentes dans vos parties. Attention tout de même à surveiller votre carburant. L’attirail est si lourd qu’un tout petit saut démarre votre jetpack. Le rechargement du carburant se fait en restant sur le sol.
> 

# 🗺️ Minimap

> La minimap (ou petite carte) logé dans le coin supérieur gauche sert à vous repérer dans l’environnement et détecter vos ennemis. En effet, tout point rouge désigne un adversaire à éliminer.
> 

# Autre

---

# 🟦 Réalisation

> Ce jeu est réalisé dans le cadre d’un projet dans le cours de Programmation Orientée Objet de la 1ère année de formation en Ingénierie Informatique du CNAM Grand-Est, créé avec Unity sur une durée de 6 mois sans initiation à l’outil.
> 

> Une première phase fut consacrée à la découverte de l'outil, quelques essais et recherches puis un débat collectif sur la direction du projet.
> 

> Ensuite, nous nous sommes réparti des tâches afin d'avancer de concert grâce notamment à git et Gitlab ainsi que Discord pour la communication. Nous avons fonctionné par système de feature, chacun réalisait des tâches correspondant plus ou moins à une fonctionnalité du jeu et livrait les modifications afin de pouvoir les intégrer au projet après revue collective.
> 

> Pour finir ce projet, nous avons créé ce document et un installateur du jeu pour notre client.
client = professeur
> 

---

# 🚩 Résultat

> Nous sommes satisfaits du jeu que nous avons réussi à produire. Il répond aux attentes de la demande et nous a laissé une part conséquente de liberté de conception.
> 

> Ce projet fut une opportunité pour notre groupe de découvrir et d’appréhender une situation de travail en équipe concrète et nous a apporté une expérience certaine à ce sujet ainsi qu’en réalisation de jeu vidéo. Ce fut également une façon de découvrir les difficultés liées au projet, au groupe et au temps, ce qui est directement en lien avec notre formation et pourra certainement nous aider dans nos avenirs respectifs.
> 

---