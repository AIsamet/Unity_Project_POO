using UnityEngine;

public class PlayerScoreboardItem : MonoBehaviour
{
	[SerializeField]
	UnityEngine.UI.Text usernameText;
	
	[SerializeField]
	UnityEngine.UI.Text killsText;

	[SerializeField]
	UnityEngine.UI.Text deathsText;

	public void Setup(Player player)
	{
		usernameText.text = player.username;
		killsText.text = "Kills: " + player.kills;
		deathsText.text = "Deaths: " + player.deaths;
	}
}
