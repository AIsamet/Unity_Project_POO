using Mirror;
using UnityEngine;

public class TimerManager : NetworkBehaviour
{
    public float gameDuration = 300f; // Dur�e de la partie en secondes

    [SyncVar]
    public float remainingTime; // Temps restant avant la fin de la partie

    public bool isRunning = false; // Indique si le timer est en cours d'ex�cution

    public override void OnStartServer()
    {
        base.OnStartServer();

        // V�rifier si l'h�te est pr�sent au d�marrage du serveur
        if (isServer && NetworkServer.localClientActive)
        {
            StartTimer();
        }
    }

    public override void OnStopServer()
    {
        base.OnStopServer();

        // Arr�ter le timer lorsque l'h�te se d�connecte
        if (isRunning)
        {
            StopTimer();
        }
    }

    private void StartTimer()
    {
        if (!isServer)
        {
            return;
        }

        remainingTime = gameDuration;
        isRunning = true;

        InvokeRepeating(nameof(UpdateTimer), 1f, 1f); // Appeler la m�thode UpdateTimer chaque seconde
    }

    private void UpdateTimer()
    {
        remainingTime--;

        if (remainingTime <= 0)
        {
            StopTimer();
            EndGame();
        }
    }

    private void StopTimer()
    {
        if (!isServer)
        {
            return;
        }

        isRunning = false;

        CancelInvoke(nameof(UpdateTimer)); // Annuler l'appel r�p�t� de la m�thode UpdateTimer
    }

    private void EndGame()
    {
        // Code pour mettre fin � la partie
        Debug.Log("Partie terminée !");
    }
}
