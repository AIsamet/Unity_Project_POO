using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class GameManager : MonoBehaviour
{
    private const string PLAYER_ID_PREFIX = "Player";
    
    private static Dictionary<string, Player> players = new Dictionary<string, Player>();

    public MatchSettings MatchSettings;

    public static GameManager instance;

    [SerializeField]
    private GameObject sceneCamera;

    public delegate void OnPlayerKilledCallback(string player, string source);
    public OnPlayerKilledCallback onPlayerKilledCallback;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            return;
        }

        Debug.LogError("More than one GameManager in scene.");
    }

    public void SetSceneCameraActive(bool isActive)
    {
        if (sceneCamera == null)
            return;
        sceneCamera.SetActive(isActive);
    }

    public static void RegisterPlayer(string netID, Player player)
    {
        string playerID = PLAYER_ID_PREFIX + netID;
        players.Add(playerID, player);
        player.transform.name = playerID;
    }
    
    public static void UnRegisterPlayer(string playerID)
    {
        players.Remove(playerID);
    }
    
    public static Player GetPlayer(string playerID)
    {
        return players[playerID];
    }

	public static Player[] GetAllPlayers()
	{
		return players.Values.ToArray();
	}    
}
