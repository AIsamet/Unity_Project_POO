using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UserAccountManager : MonoBehaviour
{
    public static UserAccountManager instance;

    public static string LoggedIn_Username;

    public string lobbySceneName = "Lobby";

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
            return;
        }
        instance = this;
        DontDestroyOnLoad(this);
    }

    public void LogIn(TextMeshProUGUI username)
    {
        LoggedIn_Username = username.text;
        Debug.Log("Logged in as " + LoggedIn_Username);
        SceneManager.LoadScene(lobbySceneName);
    }
}
