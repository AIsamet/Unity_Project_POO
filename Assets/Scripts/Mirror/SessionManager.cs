using System;
using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SessionManager : NetworkBehaviour
{
    private NetworkManager networkManager;
    private UserAccountManager userAccountManager;

    private void Start()
    {
        networkManager = NetworkManager.singleton;
        userAccountManager = UserAccountManager.instance;
    }
    

    public void LogOut()
    {

        if (isClient && isServer)
        {
            networkManager.StopHost();
            Debug.Log("Logged out host everyone");
        }
        
        //Destroy(NetworkManager.singleton);
        Cursor.lockState = CursorLockMode.None;
    }
    
    public void GoChangeUsername()
    {
        UserAccountManager.LoggedIn_Username = null;
        Destroy(UserAccountManager.instance);
        SceneManager.LoadScene("Login");
    }

}