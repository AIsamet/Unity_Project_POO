﻿using UnityEngine;

[RequireComponent(typeof(PlayerMotor))]
[RequireComponent (typeof(ConfigurableJoint))]
public class PlayerController : MonoBehaviour
{
    [SerializeField]
    private float speed = 3f;

    [SerializeField]
    private float mouseSensitivityX = 3f;

    [SerializeField]
    private float mouseSensitivityY = 3f;

    [SerializeField]
    private float thrusterForce = 1000f;

	[SerializeField]
	private float thrusterFuelBurnSpeed = 1f;

	[SerializeField]
	private float thrusterFuelRegenSpeed = 0.3f;

	private float thrusterFuelAmount = 1f;

    [Header("Camera")]
    public Camera mainCamera;
    [Header("Camera FOV Settings")]
    public float zoomedFOV;
    public float defaultFOV;
    [Tooltip("How fast the camera zooms in")]
    public float fovSpeed;

    public Animator anim;

    public float GetThrusterFuelAmount()
	{
		return thrusterFuelAmount;
	}

    [Header("Joint Options")]
    [SerializeField]
    private float jointSpring = 20;
    [SerializeField]
    private float jointMaxForce = 100;

    private PlayerMotor motor;
    private ConfigurableJoint joint;

    private void Start()
    {
        anim = gameObject.GetComponent<Animator>();
        motor = GetComponent<PlayerMotor>();
        joint = GetComponent<ConfigurableJoint>();
        SetJointSettings(jointSpring);
    }

    public void DisableController()
    {
        this.enabled = false;
    }

    private void Update()
    {
        if (PauseMenu.isOn)
        {
            if (Cursor.lockState != CursorLockMode.None)
            {
                Cursor.lockState = CursorLockMode.None;
            }

            motor.Move(Vector3.zero);
            motor.Rotate(Vector3.zero);
            motor.RotateCamera(0f);
            motor.ApplyThruster(Vector3.zero);

            return;
        }

        if (Cursor.lockState != CursorLockMode.Locked)
        {
            Cursor.lockState = CursorLockMode.Locked;
        }

        float xMov = Input.GetAxis("Horizontal");
        float zMov = Input.GetAxis("Vertical");

        Vector3 movHorizontal = transform.right * xMov;
        Vector3 movVertical = transform.forward * zMov;

        // run 
        if (Input.GetKey(KeyCode.LeftShift))
        {
            speed = 5f;
        }
        else
        {
            speed = 3f;
        }

        Vector3 velocity = (movHorizontal + movVertical) * speed;

        motor.Move(velocity);

        // Calculate player rotation as a 3D vector (turning around)
        float yRot = Input.GetAxisRaw("Mouse X");

        Vector3 rotation = new Vector3(0f, yRot, 0f) * mouseSensitivityX;

        motor.Rotate(rotation);

        // Calculate camera rotation as a 3D vector (turning around)
        float xRot = Input.GetAxisRaw("Mouse Y");

        float cameraRotationX = xRot * mouseSensitivityY;

        motor.RotateCamera(cameraRotationX);

        // Calculate thruster force
        Vector3 thrusterVelocity = Vector3.zero;
        if(Input.GetButton("Jump") && thrusterFuelAmount > 0f)
        {
			thrusterFuelAmount -= thrusterFuelBurnSpeed * Time.deltaTime;

			if(thrusterFuelAmount >= 0.01f)
			{
				thrusterVelocity = Vector3.up * thrusterForce;
				SetJointSettings(0f);
			}
        }
        else
        {
			thrusterFuelAmount += thrusterFuelRegenSpeed * Time.deltaTime;

            SetJointSettings(jointSpring);
        }

		thrusterFuelAmount = Mathf.Clamp(thrusterFuelAmount, 0f, 1f);

        // Apply thruster force
        motor.ApplyThruster(thrusterVelocity);

        // Zoom in/out with right click
        RightClickAim();

        // Player animation
        PlayerAnimation();
    }

    private void SetJointSettings(float _jointSpring)
    {
        joint.yDrive = new JointDrive
        {
            positionSpring = _jointSpring,
            maximumForce = jointMaxForce
        };
    }

    private void RightClickAim()
    {
        if (Input.GetMouseButton(1))
        {
            //Increase camera field of view
            mainCamera.fieldOfView = Mathf.Lerp(mainCamera.fieldOfView,
                zoomedFOV, fovSpeed * Time.deltaTime);
        }
        else
        {
            //Restore camera field of view
            mainCamera.fieldOfView = Mathf.Lerp(mainCamera.fieldOfView,
                defaultFOV, fovSpeed * Time.deltaTime);
        }
    }

    private void PlayerAnimation()
    {
        //Run forward
        if (Input.GetKey(KeyCode.Z))
        {
            anim.SetFloat("Vertical", 1.0f, 0, Time.deltaTime);
            anim.SetFloat("Horizontal", 0.0f, 0, Time.deltaTime);
        }
        //Run 45 up right
        /*if (Input.GetKeyDown(KeyCode.E))
        {
            anim.SetFloat("Vertical", 1.0f, 0, Time.deltaTime);
            anim.SetFloat("Horizontal", 1.0f, 0, Time.deltaTime);
        }*/
        //Run strafe right
        else if (Input.GetKey(KeyCode.D))
        {
            anim.SetFloat("Vertical", 0.0f, 0, Time.deltaTime);
            anim.SetFloat("Horizontal", 1.0f, 0, Time.deltaTime);
        }
        //Run 45 back right
        /*if (Input.GetKeyDown(KeyCode.X))
        {
            anim.SetFloat("Vertical", -1.0f, 0, Time.deltaTime);
            anim.SetFloat("Horizontal", 1.0f, 0, Time.deltaTime);
        }*/
        //Run backwards
        else if (Input.GetKey(KeyCode.S))
        {
            anim.SetFloat("Vertical", -1.0f, 0, Time.deltaTime);
            anim.SetFloat("Horizontal", 0.0f, 0, Time.deltaTime);
        }
        //Run 45 back left
        /*if (Input.GetKeyDown(KeyCode.Z))
        {
            anim.SetFloat("Vertical", -1.0f, 0, Time.deltaTime);
            anim.SetFloat("Horizontal", -1.0f, 0, Time.deltaTime);
        }*/
        //Run strafe left
        else if (Input.GetKey(KeyCode.Q))
        {
            anim.SetFloat("Vertical", 0.0f, 0, Time.deltaTime);
            anim.SetFloat("Horizontal", -1.0f, 0, Time.deltaTime);
        }
        /*//Run 45 up left
        if (Input.GetKeyDown(KeyCode.Q))
        {
            anim.SetFloat("Vertical", 1.0f, 0, Time.deltaTime);
            anim.SetFloat("Horizontal", -1.0f, 0, Time.deltaTime);
        }*/
        else
        {
            anim.SetFloat("Vertical", 0.0f, 0, Time.deltaTime);
            anim.SetFloat("Horizontal", 0.0f, 0, Time.deltaTime);
        }

    }
}
