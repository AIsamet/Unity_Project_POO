using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "WeaponData", menuName = "My Game/Weapon Date")]
public class WeaponData : ScriptableObject
{
    public string name = "Assault Rifle";
    public float damage = 20f;
    public float range = 100f;

    public float fireRate = 0f;

    public int magazineSize= 30;
    public float reloadTime = 1f;

    public GameObject graphics;
}
