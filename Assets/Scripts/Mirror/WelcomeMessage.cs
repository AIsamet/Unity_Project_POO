using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class WelcomeMessage : MonoBehaviour
{
    [SerializeField] 
    private TextMeshProUGUI welcomeText;

    void Start()
    {
        SetText();
    }

    void SetText()
    {
        welcomeText.text = "Welcome " + UserAccountManager.LoggedIn_Username.ToString() + " !";
    }
}
