using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour
{
	[SerializeField]
	private RectTransform thrusterFuelFill;

    [SerializeField]
    private RectTransform healthBarFill;

    private PlayerController controller;
    private Player player;
	private WeaponManager weaponManager;

	[SerializeField]
	private Text ammoText;

	[SerializeField]
	public GameObject scoreboard;

    [SerializeField]
    private GameObject pauseMenu;

	[SerializeField]
	private TextMeshProUGUI timer;

	private TimerManager timeManager;
	private SessionManager sessionManager;

    public void SetPlayer(Player _player)
	{
		player = _player;
		controller = player.GetComponent<PlayerController>();
		weaponManager = player.GetComponent<WeaponManager>();
	}
	
	private void Start()
	{
        PauseMenu.isOn = false;
		timeManager = GameObject.FindObjectOfType<TimerManager>();
		sessionManager = GameObject.FindObjectOfType<SessionManager>();
    }

	private void Update()
	{
		SetFuelAmount(controller.GetThrusterFuelAmount());
		SetHealthAmount(player.GetHealthPct());
		SetAmmoAmount(weaponManager.currentMagazineSize);

		timer.text = timeManager.remainingTime.ToString();

        if(timeManager.remainingTime <= 0)
		{
			scoreboard.SetActive(true);
			StartCoroutine(LoadLoginSceneAfterDelay(6));
			return;
        }



        if (Input.GetKeyDown(KeyCode.Escape))
		{
			TogglePauseMenu();
		}

		if (Input.GetKeyDown(KeyCode.Tab))
		{
			scoreboard.SetActive(true);
		}
		else if (Input.GetKeyUp(KeyCode.Tab))
		{
			scoreboard.SetActive(false);
		}

		
	}

	public void TogglePauseMenu()
	{
		pauseMenu.SetActive(!pauseMenu.activeSelf);
		PauseMenu.isOn = pauseMenu.activeSelf;
	}

	void SetFuelAmount(float _amount)
	{
		thrusterFuelFill.localScale = new Vector3(1f, _amount, 1f);
	}

	void SetHealthAmount(float _amount)
	{
        healthBarFill.localScale = new Vector3(1f, _amount, 1f);
    }

	void SetAmmoAmount(int _amount)
	{
		ammoText.text = _amount.ToString();
	}

    private IEnumerator LoadLoginSceneAfterDelay(float delay)
    {
        yield return new WaitForSeconds(delay);
        sessionManager.LogOut();
    }

}
