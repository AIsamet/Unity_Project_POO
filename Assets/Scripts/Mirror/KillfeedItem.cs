using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class KillfeedItem : MonoBehaviour
{

    [SerializeField]
    private TextMeshProUGUI text;
    public void Setup(string player, string source)
    {
        text.text = "<b>" + source + "</b>" + " killed " + player;
    }
}
