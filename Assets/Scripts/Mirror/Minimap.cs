using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class Minimap : NetworkBehaviour
{
    [SerializeField]
    private GameObject white;

    [SerializeField]
    private GameObject red;

    void Start()
    {
        if(isLocalPlayer)
        {
            red.SetActive(false);
        }  
        else
        {
            white.SetActive(false);
        }
    }
}
