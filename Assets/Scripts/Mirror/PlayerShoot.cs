using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using System.Threading;

[RequireComponent(typeof(WeaponManager))]
public class PlayerShoot : NetworkBehaviour
{

    [SerializeField]
    private Camera cam;

    [SerializeField]
    private LayerMask mask;

    private WeaponData currentWeapon;
    private WeaponManager weaponManager;
    private PlayerController playerController;

    private bool isActive = true;

    void Start()
    {
        if(cam == null) 
        {
            Debug.LogError("PlayerShoot: No camera referenced!");
            this.enabled = false;
        }

        weaponManager = GetComponent<WeaponManager>();
        playerController = GetComponent<PlayerController>();
    }

    // disable shoot 
    public void DisableShoot()
    {
        isActive = false;
        this.enabled = false;
    }

    private void Update()
    {
        currentWeapon = GetComponent<WeaponManager>().GetCurrentWeapon();

        if (PauseMenu.isOn)
        {
            return;
        }

        if (Input.GetKeyDown(KeyCode.R) && weaponManager.currentMagazineSize < currentWeapon.magazineSize)
        {
            StartCoroutine(weaponManager.Reload());
            //Play reload animation
            playerController.anim.Play("Reload", 1, 0.0f);
            return;
        }

        if (currentWeapon.fireRate <= 0f)
        {
            if (Input.GetButtonDown("Fire1"))
            {
                Shoot();
            }
        }
        else
        {
            if (Input.GetButtonDown("Fire1"))
            {
                InvokeRepeating("Shoot", 0f, 1f / currentWeapon.fireRate);
            }
            else if (Input.GetButtonUp("Fire1"))
            {
                CancelInvoke("Shoot");
            }
        }

    }

    // Is called on the server when a player shoots
    [Command]
    void CmdOnShoot()
    {
        RpcDoShootEffect();
    }

    // Is called on all clients when we need to do a shoot effect
    [ClientRpc]
    void RpcDoShootEffect()
    {
        // Play muzzle flash
        weaponManager.GetCurrentGraphics().muzzleFlash.Play();

        //Play shoot sound (bug at respawn to be fixed)
        weaponManager.GetCurrentGraphics().shootAudioSource.clip = weaponManager.GetCurrentGraphics().shootSound;
        weaponManager.GetCurrentGraphics().shootAudioSource.Play();

        //Spawn casing at spawnpoint
        Instantiate(weaponManager.GetCurrentGraphics().casingPrefab,
        weaponManager.GetCurrentGraphics().casingSpawnpoint.transform.position,
        weaponManager.GetCurrentGraphics().casingSpawnpoint.transform.rotation);

        //Spawn bullet from bullet spawnpoint
        var bullet = (Transform)Instantiate(
            weaponManager.GetCurrentGraphics().bulletPrefab,
            weaponManager.GetCurrentGraphics().bulletSpawnpoint.transform.position,
            weaponManager.GetCurrentGraphics().bulletSpawnpoint.transform.rotation);

        //Add velocity to the bullet
        bullet.GetComponent<Rigidbody>().velocity =
            bullet.transform.forward * weaponManager.GetCurrentGraphics().bulletForce;
    }

    [Client]
    private void Shoot()
    {
        if (isActive)
        {
            if (!isLocalPlayer || weaponManager.isReloading)
            {
                return;
            }

            if (weaponManager.currentMagazineSize <= 0)
            {
                StartCoroutine(weaponManager.Reload());
                playerController.anim.Play("Reload", 1, 0.0f);
                return;
            }

            Debug.Log("Shoot");
            weaponManager.currentMagazineSize--;
            Debug.Log("Ammo: " + weaponManager.currentMagazineSize);

            // We are shooting, call the OnShoot method on the server
            CmdOnShoot();

            RaycastHit hit;
            if (Physics.Raycast(cam.transform.position, cam.transform.forward, out hit, currentWeapon.range, mask))
            {
                if (hit.collider.tag == "Player")
                {
                    CmdPlayerShot(hit.collider.name, currentWeapon.damage, transform.name);
                }
            }

            if (weaponManager.currentMagazineSize <= 0)
            {
                StartCoroutine(weaponManager.Reload());
                playerController.anim.Play("Reload", 1, 0.0f);
                return;
            }
        }
        else
        {
            return;
        }
    }

    [Command]
    private void CmdPlayerShot(string playerId, float damage, string sourceID)
    {
        Debug.Log(playerId + " has been shot.");
        
        Player player = GameManager.GetPlayer(playerId);
        player.RPCTakeDamage(damage, sourceID);
    }
}
