using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scoreboard : MonoBehaviour
{
	[SerializeField]
	GameObject playerScoreboardItem;

	[SerializeField]
	Transform playerScoreboardList;

	void OnEnable()
	{
		Player[] players = GameManager.GetAllPlayers();

		foreach(var player in players)
		{
			GameObject itemGO = Instantiate(playerScoreboardItem, playerScoreboardList);
			PlayerScoreboardItem item = itemGO.GetComponent<PlayerScoreboardItem>();
			if(item != null)
			{
				item.Setup(player);
			}
		}
	}

	void OnDisable()
	{
		foreach(Transform child in playerScoreboardList)
		{
			Destroy(child.gameObject);
		}
	}
}
