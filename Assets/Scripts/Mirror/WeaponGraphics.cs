using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponGraphics : MonoBehaviour
{
    [Header("Weapon Components")]
    public ParticleSystem muzzleFlash;
    public Light muzzleflashLight;
    public GameObject hitEffectPrefab;

    [Header("Prefabs")]
    public Transform casingPrefab;
    public Transform bulletPrefab;
    public float bulletForce;
    //public Transform grenadePrefab;
    //public float grenadeSpawnDelay;

    [Header("Spawnpoints")]
    public Transform casingSpawnpoint;
    public Transform bulletSpawnpoint;
    //public Transform grenadeSpawnpoint;

    [Header("Audio Clips")]
    public AudioClip shootSound;

    [Header("Audio Sources")]
    public AudioSource shootAudioSource;
}
